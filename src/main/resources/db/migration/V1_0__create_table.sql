CREATE TABLE safety_procedure (
	id INTEGER NOT NULL AUTO_INCREMENT,
	type CHAR(1) NOT NULL,
	description VARCHAR(500) NOT NULL,
	creation_date TIMESTAMP NOT NULL,
	update_date TIMESTAMP NULL,
	CONSTRAINT pk_safety_procedure PRIMARY KEY (id)
);