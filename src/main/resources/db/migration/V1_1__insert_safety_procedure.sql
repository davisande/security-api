INSERT INTO safety_procedure(type, description, creation_date, update_date)
VALUES ('I', 'Barragem com alto risco de rompimento. Vá imediatamente para o ponto de encontro',
NOW(), null);