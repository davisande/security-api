package com.tccpuc.securityapi.sendnotification;

import com.tccpuc.securityapi.sendnotification.exception.FailedSendNotificationException;
import com.tccpuc.securityapi.sendnotification.mapper.SendNotificationRequestMapper;
import com.tccpuc.securityapi.sendnotification.mapper.SentMessageMapper;
import com.tccpuc.securityapi.sendnotification.dto.SendNotificationRequest;
import com.tccpuc.securityapi.sendnotification.dto.SendNotificationResponse;
import com.tccpuc.securityapi.sendnotification.service.SendNotificationService;
import java.util.Optional;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
public class SendNotificationController {

  private final SendNotificationRequestMapper sendNotificationRequestMapper;
  private final SentMessageMapper sentMessageMapper;
  private final SendNotificationService sendNotificationService;

  @PostMapping("/communications/send-notification")
  public SendNotificationResponse sendNotification(
      @RequestBody @Valid final SendNotificationRequest sendNotificationRequest) {
    return Optional.of(sendNotificationRequest)
        .map(sendNotificationRequestMapper::toSendNotification)
        .map(sendNotificationService::send)
        .map(sentMessageMapper::toSendNotificationResponse)
        .orElseThrow(FailedSendNotificationException::new);
  }
}
