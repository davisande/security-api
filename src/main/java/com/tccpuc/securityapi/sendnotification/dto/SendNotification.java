package com.tccpuc.securityapi.sendnotification.dto;

import com.tccpuc.securityapi.core.enums.SafetyProcedureTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SendNotification {

  private SafetyProcedureTypeEnum type;
  private String sensorLocalization;
  private String monitoringEventDescription;

}
