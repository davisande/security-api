package com.tccpuc.securityapi.sendnotification.dto;

import lombok.Data;

@Data
public class SendNotificationResponse {

  private String title;
  private String body;
  private String icon;
  private String click_action;

}
