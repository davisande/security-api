package com.tccpuc.securityapi.sendnotification.dto;

import com.tccpuc.securityapi.core.enums.SafetyProcedureTypeEnum;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class SendNotificationRequest {

  @Enumerated(EnumType.STRING)
  private SafetyProcedureTypeEnum type;

  @NotEmpty
  private String sensorLocalization;

  @NotEmpty
  private String monitoringEventDescription;

}
