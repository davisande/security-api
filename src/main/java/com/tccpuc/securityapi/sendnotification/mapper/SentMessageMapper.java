package com.tccpuc.securityapi.sendnotification.mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

import com.tccpuc.securityapi.integration.firebase.request.Notification;
import com.tccpuc.securityapi.sendnotification.dto.SentMessage;
import com.tccpuc.securityapi.sendnotification.dto.SendNotificationResponse;
import org.mapstruct.Mapper;

@Mapper(unmappedTargetPolicy = IGNORE)
public interface SentMessageMapper {

  SentMessage toSentMessage(Notification notification);

  SendNotificationResponse toSendNotificationResponse(SentMessage sentMessage);

}
