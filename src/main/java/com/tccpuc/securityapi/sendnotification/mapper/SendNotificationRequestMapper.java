package com.tccpuc.securityapi.sendnotification.mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

import com.tccpuc.securityapi.sendnotification.dto.SendNotification;
import com.tccpuc.securityapi.sendnotification.dto.SendNotificationRequest;
import org.mapstruct.Mapper;

@Mapper(unmappedTargetPolicy = IGNORE)
public interface SendNotificationRequestMapper {

  SendNotification toSendNotification(SendNotificationRequest sendNotificationRequest);

}
