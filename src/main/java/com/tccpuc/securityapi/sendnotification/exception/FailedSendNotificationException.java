package com.tccpuc.securityapi.sendnotification.exception;

public class FailedSendNotificationException extends RuntimeException {
  public static final String DEFAULT_MESSAGE = "The send notification was fail";

  public FailedSendNotificationException() {
    super(DEFAULT_MESSAGE);
  }
}
