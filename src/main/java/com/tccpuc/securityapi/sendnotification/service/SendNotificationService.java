package com.tccpuc.securityapi.sendnotification.service;

import static java.util.Optional.of;
import static java.util.Optional.ofNullable;

import com.tccpuc.securityapi.integration.firebase.FirebaseClient;
import com.tccpuc.securityapi.integration.firebase.GoogleApplicationCredential;
import com.tccpuc.securityapi.integration.firebase.request.Message;
import com.tccpuc.securityapi.integration.firebase.request.Notification;
import com.tccpuc.securityapi.integration.firebase.request.PushNotificationRequest;
import com.tccpuc.securityapi.sendnotification.dto.SendNotification;
import com.tccpuc.securityapi.sendnotification.dto.SentMessage;
import com.tccpuc.securityapi.sendnotification.exception.FailedSendNotificationException;
import com.tccpuc.securityapi.sendnotification.mapper.SentMessageMapper;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class SendNotificationService {
  private static final String NOTIFICATION_TITLE = "Evento de Monitoramento";
  private static final String TOPIC = "monitoring_event";

  private final SentMessageMapper sentMessageMapper;
  private final GoogleApplicationCredential googleApplicationCredential;
  private final FirebaseClient firebaseClient;

  public SentMessage send(@NonNull final SendNotification sendNotification) {
    return ofNullable(sendNotification)
        .map(this::performSendPushNotification)
        .orElseThrow(FailedSendNotificationException::new);
  }

  private SentMessage performSendPushNotification(final SendNotification sendNotification) {
    return of(buildPushNotificationRequest(sendNotification))
        .map(this::performSendPushNotification)
        .map(pnr -> sentMessageMapper.toSentMessage(pnr.getMessage().getNotification()))
        .orElseThrow(FailedSendNotificationException::new);
  }

  private PushNotificationRequest buildPushNotificationRequest(
      final SendNotification sendNotification) {

    final Notification notification = Notification.builder()
        .title(NOTIFICATION_TITLE)
        .body(sendNotification.getMonitoringEventDescription())
        .build();

    final Message message = Message.builder()
        .topic(TOPIC)
        .notification(notification)
        .build();

    return PushNotificationRequest.builder()
        .validateOnly(false)
        .message(message)
        .build();
  }

  private PushNotificationRequest performSendPushNotification(
      final PushNotificationRequest pushNotificationRequest) {
    ofNullable(String.format("Bearer %s", googleApplicationCredential.getAccessToken()))
        .ifPresent(token -> firebaseClient.sendPushNotification(token, pushNotificationRequest));

    return pushNotificationRequest;
  }
}
