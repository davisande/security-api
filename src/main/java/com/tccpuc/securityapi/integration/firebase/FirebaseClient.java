package com.tccpuc.securityapi.integration.firebase;

import com.tccpuc.securityapi.integration.firebase.request.PushNotificationRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "firebase", url = "${integration.firebase.url}")
public interface FirebaseClient {

  @RequestMapping(method = RequestMethod.POST, value = "${integration.firebase.send-resource}")
  void sendPushNotification(@RequestHeader("Authorization") String authorization,
      @RequestBody PushNotificationRequest pushNotificationRequest);

}
