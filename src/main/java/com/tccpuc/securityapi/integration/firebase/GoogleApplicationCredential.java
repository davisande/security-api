package com.tccpuc.securityapi.integration.firebase;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.tccpuc.securityapi.integration.firebase.exception.GoogleApplicationCredentialException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class GoogleApplicationCredential {

  @Value("${integration.firebase.credentials-url}")
  private String messagingScope;

  public String getAccessToken() {
    final String credentialsFilePath = System.getenv().get("GOOGLE_APPLICATION_CREDENTIALS");

    try {
      final GoogleCredential googleCredential = GoogleCredential
          .fromStream(new FileInputStream(credentialsFilePath))
          .createScoped(Arrays.asList(messagingScope));
      googleCredential.refreshToken();

      return googleCredential.getAccessToken();

    } catch (IOException e) {
      throw new GoogleApplicationCredentialException();
    }
  }

}
