package com.tccpuc.securityapi.integration.firebase.exception;

public class GoogleApplicationCredentialException extends RuntimeException {
  public static final String DEFAULT_MESSAGE = "Error to get Google credentials";

  public GoogleApplicationCredentialException() {
    super(DEFAULT_MESSAGE);
  }

}
