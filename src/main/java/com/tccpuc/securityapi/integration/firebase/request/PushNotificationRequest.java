package com.tccpuc.securityapi.integration.firebase.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PushNotificationRequest {

  @JsonProperty("validate_only")
  private Boolean validateOnly;

  private Message message;

}
