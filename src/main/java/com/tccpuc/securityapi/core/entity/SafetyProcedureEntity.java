package com.tccpuc.securityapi.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import com.tccpuc.securityapi.core.enums.SafetyProcedureTypeEnum;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@Entity
@Table(name = "safety_procedure")
@EntityListeners(AuditingEntityListener.class)
public class SafetyProcedureEntity {

  @Id
  @GeneratedValue(strategy = IDENTITY)
  private Integer id;

  @Enumerated(EnumType.STRING)
  private SafetyProcedureTypeEnum type;

  @Column
  private String description;

  @CreatedDate
  @Column(name = "creation_date")
  private Instant creationDate;

  @LastModifiedDate
  @Column(name = "update_date")
  private Instant updateDate;

}
