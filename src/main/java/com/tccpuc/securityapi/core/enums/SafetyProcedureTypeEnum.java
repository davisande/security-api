package com.tccpuc.securityapi.core.enums;

import static lombok.AccessLevel.PRIVATE;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = PRIVATE)
public enum SafetyProcedureTypeEnum {
  W("Warn"),
  B("Break"),
  I("Dam Instability");

  private String description;
}
