package com.tccpuc.securityapi.core.repository;

import com.tccpuc.securityapi.core.entity.SafetyProcedureEntity;
import com.tccpuc.securityapi.core.enums.SafetyProcedureTypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SafetyProcedureRepository extends JpaRepository<SafetyProcedureEntity, Integer> {

  SafetyProcedureEntity findByType(SafetyProcedureTypeEnum type);

}
